local storage = minetest.get_mod_storage()
local __question_data
local __player_data

function testmod.save()
	storage:set_string("question_data",minetest.serialize(__question_data))
	storage:set_string("player_data",minetest.serialize(__player_data))
end

function testmod.load()
	__question_data = minetest.deserialize(storage:get_string("question_data")) or {}
	__player_data = minetest.deserialize(storage:get_string("player_data")) or {}
end

function testmod.getqdata(question)
	local qdata = __question_data[question] or {}
	__question_data[question] = qdata
	qdata.info = qdata.info or ""
	
	return qdata
end

function testmod.getpdata(name)
	local pdata = __player_data[name] or {}
	__player_data[name] = pdata
	pdata.done = pdata.done or ""
	
	return pdata
end
