minetest.register_chatcommand("qdef", {
	params = "<question> <info>",
	description = "put info in question",
	func = function(sender,param)
			local question, info = string.match(param,"([%g]+) ([%g]+)")
			minetest.chat_send_all("question : "..question.." info : "..info)
			local qdef = testmod.getqdata(question)
			qdef.info = info
			testmod.save()
		end,
	})
	
minetest.register_chatcommand("pdef", {
	params = "<question>",
	description = "show info for question",
	func = function(sender,question)
			local qdef = testmod.getqdata(question)
			minetest.chat_send_all(dump(qdef))
		end,
	})
	

minetest.register_chatcommand("qdo", {
	params = "<player> <done>",
	description = "put done in player",
	func = function(sender,param)
			local plname, done = string.match(param,"([%g]+) ([%g]+)")
			minetest.chat_send_all("player : "..plname.." done : "..done)
			local pdef = testmod.getpdata(plname)
			local question = "q1"
			local qdef = testmod.getqdata(question)
			qdef.info = done
			pdef.done = done
			testmod.save()
		end,
	})
	
minetest.register_chatcommand("pdo", {
	params = "<plname>",
	description = "show done for player",
	func = function(sender,plname)
			local pdo = testmod.getpdata(plname)
			minetest.chat_send_all(dump(pdo))
		end,
	})
